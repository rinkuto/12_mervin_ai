﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "PluggableAI/Action/Chase")]
public class ChaseAction : Action {

    public override void Act(StateController controller)
    {
        
    }
    public void Chase(StateController controller)
    {
        controller.navMeshAgent.destination = controller.ChaseTarget.position;
        controller.navMeshAgent.Resume();
    }
}
