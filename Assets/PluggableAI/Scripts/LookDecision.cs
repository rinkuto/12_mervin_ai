﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu ( menuName = "PluggableAI/Decisions/look")]
public class LookDecision : Decision{
    // Use this for initialization
    public override bool Decide(StateController controller)
    {
        bool targetVisible = Look(controller);
        return targetVisible; 
    }
    private bool Look(StateController controller)
    {
        RaycastHit hit;

        Debug.DrawRay(controller.eyes.position, controller.eyes.forward.normalized * controller.enemyStats.lookRange, Color.green);

        if (Physics.SphereCast(controller.eyes.position, controller.enemyStats.lookSphereCastRadius, controller.eyes.forward, out hit, controller.enemyStats.lookRange) && hit.collider.CompareTag("Player"))
        {
            controller.ChaseTarget = hit.transform;

            return true;
        }
        else
        {
            return false;
        }
            
    }
}
