﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/State")]
public class State : ScriptableObject {


    public Action[] actions;
    public Transition[] transitions;
    public Color SceneGizmoColor = Color.grey;
	// Use this for initialization
    public void updateState(StateController controller)
    {
        DoActions(controller);
        checkTransition(controller);

    }
    private void DoActions(StateController controller)
    {
        for (int i = 0; i < actions.Length; i++)
        {
            actions[i].Act(controller);
        }
    }
    private void checkTransition(StateController controller)
    {
        for (int i = 0; i < transitions.Length; i++)
        {
            bool decisionSucceded = transitions[i].decision.Decide(controller);
                
            if(decisionSucceded)
            {
                controller.TransitionToState(transitions[i].TrueState);
            }else
            {
                controller.TransitionToState(transitions[i].FalseState);
            }
        }
    }
   
}
