﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Action/Patrol")]
public class PatrolAction : Action 
{
    public override void Act(StateController controller)
    {
        patrol(controller);
    }
    private void patrol(StateController controller)
    {
        controller.navMeshAgent.destination = controller.wayPointList[controller.NextWayPoint].position;
        controller.navMeshAgent.Resume();
        if(controller.navMeshAgent.remainingDistance <= controller.navMeshAgent.stoppingDistance && !controller.navMeshAgent.pathPending)
        {
            controller.NextWayPoint = (controller.NextWayPoint + 1) % controller.wayPointList.Count;
        }
    }
}
